package chapter6.beans;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

public class User implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String account;
    private String name;
    private String email;
    private String password;
    private String description;
    private Date createdDate;
    private Date updatedDate;

    public int getId() {
    	return this.id;
    }
	public void setId(int id) {
		// TODO 自動生成されたメソッド・スタブ
		this.id = id;
	}
	public String getAccount() {
		// TODO 自動生成されたメソッド・スタブ
		return this.account;
	}
	public void setAccount(String parameter) {
		// TODO 自動生成されたメソッド・スタブ
		this.account = parameter;
	}
	public String getName() {
		// TODO 自動生成されたメソッド・スタブ
		return this.name;
	}
	public void setName(String parameter) {
		// TODO 自動生成されたメソッド・スタブ
		this.name = parameter;
	}
	public String getEmail() {
		// TODO 自動生成されたメソッド・スタブ
		return this.email;
	}
	public void setEmail(String parameter) {
		// TODO 自動生成されたメソッド・スタブ
		this.email = parameter;
	}
	public String getPassword() {
		// TODO 自動生成されたメソッド・スタブ
		return this.password;
	}
	public void setPassword(String parameter) {
		// TODO 自動生成されたメソッド・スタブ
		this.password = parameter;
	}
	public String getDescription() {
		// TODO 自動生成されたメソッド・スタブ
		return this.description;
	}
	public void setDescription(String parameter) {
		// TODO 自動生成されたメソッド・スタブ
		this.description = parameter;
	}
	public Date getCreatedDate() {
		// TODO 自動生成されたメソッド・スタブ
		return this.createdDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		// TODO 自動生成されたメソッド・スタブ
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		// TODO 自動生成されたメソッド・スタブ
		return updatedDate;
	}
	public void setUpdatedDate(Timestamp updatedDate) {
		// TODO 自動生成されたメソッド・スタブ
		this.updatedDate = updatedDate;
	}

}