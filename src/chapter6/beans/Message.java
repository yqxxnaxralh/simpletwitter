package chapter6.beans;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

public class Message implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private int userId;
    private String text;
    private Timestamp createdDate;
    private Timestamp updatedDate;

    public int getid() {
    	return this.id;
    }
    public void setid(int id) {
    	this.id = id;
    }
	public int getUserId() {
		// TODO 自動生成されたメソッド・スタブ
		return this.userId;
	}
	public void setUserId(int userId) {
		// TODO 自動生成されたメソッド・スタブ
		this.userId = userId;
	}
	public String getText() {
		// TODO 自動生成されたメソッド・スタブ
		return this.text;
	}
	public void setText(String text) {
		// TODO 自動生成されたメソッド・スタブ
		this.text = text;
	}
	public Date getcreatedDate() {
		return this.createdDate;
	}
	public void setcreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
	public Date getupdatedDate() {
		return this.updatedDate;
	}
	public void setupdatedDate(Timestamp updatedDate) {
		this.updatedDate = updatedDate;
	}
}