package chapter6.beans;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

public class UserMessage implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String account;
    private String name;
    private int userId;
    private String text;
    private Date created_date;
    // getter setter は省略
    public int getId() {
    	return this.id;
    }
    public void setId(int id) {
    	this.id = id;
    }
    public String getAccount() {
    	return this.account;
    }
	public void setAccount(String account) {
		// TODO 自動生成されたメソッド・スタブ
		this.account = account;
	}
    public String getName() {
    	return this.name;
    }
	public void setName(String name) {
		// TODO 自動生成されたメソッド・スタブ
		this.name = name;
	}
    public int getUserId() {
    	return this.userId;
    }
	public void setUserId(int userId) {
		// TODO 自動生成されたメソッド・スタブ
		this.userId = userId;
	}
    public String getText() {
    	return this.text;
    }
	public void setText(String text) {
		// TODO 自動生成されたメソッド・スタブ
		this.text = text;
	}
    public Date getCreated_date() {
    	return this.created_date;
    }
	public void setCreated_date(Timestamp createdDate) {
		// TODO 自動生成されたメソッド・スタブ
		this.created_date = createdDate;
	}
}